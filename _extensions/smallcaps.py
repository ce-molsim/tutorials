from docutils import nodes
from docutils.parsers.rst import Directive, directives
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils.parsers.rst.directives.misc import Class
from docutils.parsers.rst.directives.misc import Include as BaseInclude
from sphinx.locale import _

from sphinx import addnodes


NBS = "\xa0"

class smallcaps(nodes.Inline, nodes.TextElement):
    pass

def visit_smallcaps_html(self, node):
    self.body.append(r'<span class="smallcaps">')

def depart_smallcaps_html(self, node):
    self.body.append(r'</span>')

def visit_smallcaps_tex(self, node):
    self.body.append(r'\textsc{')

def depart_smallcaps_tex(self, node):
    self.body.append(r'}')

def smallcaps_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    node = smallcaps()
    txt = nodes.Text(text)
    node += txt
    return [node], []


def setup(app):
    app.add_node(smallcaps,
                 html=(visit_smallcaps_html, depart_smallcaps_html),
                 latex=(visit_smallcaps_tex, depart_smallcaps_tex),
                 )
    app.add_role("smallcaps", smallcaps_role)
    app.add_css_file('css/smallcaps.css')
