from docutils import nodes
from docutils.parsers.rst import Directive, directives
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils.parsers.rst.directives.misc import Class
from docutils.parsers.rst.directives.misc import Include as BaseInclude
from sphinx.locale import _

from sphinx import addnodes


class email(nodes.General, nodes.Inline, nodes.Referential,
            nodes.TextElement):
    def __init__(self, rawsource='', text='', *children, **attributes):
        if not "refuri" in attributes:
            s_text = text.strip()
            if s_text.startswith("<"): s_text = s_text[1:]
            if s_text.endswith(">"): s_text = s_text[:-1]
            attributes["refuri"] = "mailto:"+s_text
        super().__init__(rawsource, text, *children, **attributes)
        self["refuri"] = attributes["refuri"]


def is_email(txt):
    return (txt is not None) and txt.startswith("<") and txt.endswith(">") and ("@" in txt)

def visit_email(self, node):
    self.visit_reference(node)

def depart_email(self, node):
    self.depart_reference(node)


def setup(app):
    app.add_node(email,
                 html=(visit_email, depart_email),
                 latex=(visit_email, depart_email))
