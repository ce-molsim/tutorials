
.. |~| unicode:: U+00A0
       :trim:

.. role:: prog(smallcaps)

.. role:: name(smallcaps)


.. |Balena| replace:: :name:`Balena`
.. |MOF| replace:: :smallcaps:`mof`
.. |Mof| replace:: :smallcaps:`Mof`
.. |Henry| replace:: :name:`Henry`
.. |md| replace:: molecular dynamics
.. |MD| replace:: |md|
.. |tnt| replace:: :smallcaps:`tnt`
.. |Md| replace:: Molecular dynamics
.. |LJ| replace:: :smallcaps:`Lennard-Jones`
.. |hpc| replace:: :smallcaps:`hpc`
.. |Hpc| replace:: :smallcaps:`Hpc`
.. |cli| replace:: :smallcaps:`cli`
.. |Cli| replace:: :smallcaps:`Cli`
.. |MC| replace:: :smallcaps:`Monte-Carlo`
.. |VMD| replace:: :prog:`Vmd`
.. |Crystalmaker| replace:: :prog:`CrystalMaker`
.. |Gromacs| replace:: :prog:`Gromacs`
.. |Music| replace:: :prog:`MuSiC`
.. |Raspa| replace:: :prog:`Raspa`
.. |Taskfarmer| replace:: :prog:`Taskfarmer`
.. |Putty| replace:: :prog:`Putty`
.. |Kitty| replace:: :prog:`Kitty`
.. |XMing| replace:: :prog:`Xming`
.. |Filezilla| replace:: :prog:`FileZilla`
.. |openssh| replace:: :prog:`OpenSSH`
.. |ssh| replace:: :smallcaps:`ssh`
.. |Ssh| replace:: :smallcaps:`Ssh`
.. |cpus| replace:: :smallcaps:`cpu` cores
.. |cores| replace:: |cpus|
.. |core| replace:: :smallcaps:`cpu` core
.. |Windows| replace:: :prog:`Windows`
.. |Windows10| replace:: :prog:`Windows 10`
.. |Mac| replace:: :prog:`MacOS`
.. |Linux| replace:: :prog:`Linux`
.. |Bash| replace:: :prog:`Bash`
.. |Nautilus| replace:: :prog:`Nautilus`
.. |MobaXterm| replace:: :prog:`MobaXterm`
.. |mRemoteNG| replace:: :prog:`mRemoteNG`
.. |XQuartz| replace:: :prog:`XQuartz`

