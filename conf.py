# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('_extensions'))


# -- Project information -----------------------------------------------------

project = 'CE-MolSim Training'
copyright = '2017-2024, Gaël Donval'
author = ' '

# The full version, including alpha/beta/rc tags
release = '2018'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.todo',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sectionauthors',
    'smallcaps',
    'emails',
#    'figtable',
#    'numfig',
#    'numsec',
#    'figtabular',
    'sphinxcontrib.tikz',
    'download',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']
source_suffix = ['.rst', '.md']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', '_includes', "_pages/*/*.rst", 'README.rst', 'README.md', '_extensions']
master_doc = 'index'
#language = 'en'
pygments_style = 'sphinx'
todo_include_todos = True


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'collapse_navigation': False,
    'sticky_navigation': False,
    'navigation_depth': 2,
    'display_version': False,
    'prev_next_buttons_location': "bottom",
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

show_authors = False
numfig = True
numfig_secnum_depth = 3
numfig_format = {
    "section": u"Sec.\u00A0%s",
    "figure" : u"Fig.\u00A0%s",
    "table"  : u"Tab.\u00A0%s",
    "code-block"  : u"List.\u00A0%s",
}
rst_prolog = ".. include:: /_includes/prolog.rst\n"

# This is required for the alabaster theme
# refs: http://alabaster.readthedocs.io/en/latest/installation.html#sidebars

#html_sidebars = {
#    '**': [
#        'relations.html',  # needs 'show_related': True theme option to display
#        'searchbox.html',
#    ]
#}

def read_if_exists(fname):
    from os.path import exists
    for pre in ("_includes/", ""):
        if exists(pre+fname):
            with open(pre+fname) as f:
                return f.read()
    print("No file named", fname)
    return ""



# -- Options for LaTeX output ---------------------------------------------
latex_engine = 'lualatex'
latex_docclass = {
    'manual': 'scrreprt',
    'howto': 'scrartcl',
}
latex_elements = {
    'releasename': ' ',
    'geometry': '',
    'sphinxsetup': 'verbatimwithframe=false,verbatimhintsturnover=true,VerbatimColor={rgb}{0.95,0.95,0.95}',
    'papersize': 'a5paper',
    'pointsize': '11pt',
    'fontpkg': read_if_exists("fonts.tex"),
    'preamble': read_if_exists("preamble.tex"),
    'fncychap': r'\usepackage[Bjornstrup]{fncychap}',
    #'fncychap': r'',
    'printindex': r'\footnotesize\raggedright\printindex',
}
latex_show_urls = 'footnote'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
author_tex = r" "
latex_documents = [
    (master_doc, 'CE-MolSim.tex', 'CE-MolSim Documentation',
     author_tex, 'manual'),
]

