.. _sec_CLP:

Command-line primer
===================

.. secauthor:: Tina Düren <t.duren@bath.ac.uk>
               Lev Sarkisov <Lev.Sarkisov@ed.ac.uk>
               Gaël Donval <g.donval@bath.ac.uk>

This guide quickly introduces you to working in a command-line |Linux|
environment, with elements of data transfer, plotting and computer
visualizations.

**Objectives:**

#. Getting familiar with the |Linux| environment for scientific computation;
#. Learning how to move/navigate within the file hierarchy in |Linux|; 
   create, move, rename and delete files and directories; 
   look inside and edit text files; execute commands;
#. Acquiring basic skills with graph plotting software and molecular 
   visualization software.

Just like |Windows|, |Linux| is a powerful operating system (and in contrast 
to |Windows|, it is free). |Linux| is used for the majority of scientific 
computing tasks. However, most of what you are going to do is very text-based
and takes a bit of getting used to.


Connecting to a remote |Linux| computer
---------------------------------------

Let us start.

All simulations will be performed on a remote Linux computer. First of all, you need a way to connect to a |Linux| machine. There are different ways of doing that 

* On a Mac, you can directly connect to a remote |Linux| machine using a terminal. You need to install Xquartz (https://https://www.xquartz.org/index.html/) as well.
* If you are using a Windows machine, we recommend that you download MobaXterm https://mobaxterm.mobatek.net/. You can either use a portable edition which you should use if you are using a University machine or the installer edition which you should use if you are using your own machine.
* Alternatively, as a Windows user you can also install a complete Ubuntu terminal environment on Windows with Windows Subsystem for Linux (WSL) https://ubuntu.com/wsl. Please note that you can only do that if you are working on your own machine and have administration priviliges.

Start MobaXterm (or whatever way you are using to connect to a remote Linux machine) and connect to ``linux.bath.ac.uk`` using

.. code-block:: console

    ssh username@linux.bath.ac.uk


where username is your normal University of Bath username (e.g. td222). When prompted supply your University of Bath password. 

You will now have opened a |Linux| terminal on a |Windows| machine and can 
work as if you were sitting at a |Linux| machine. The file store is the same
and you can access all your files in your :file:`H:` drive from |Linux|. 
This also means that you can access all files created in the |Linux| 
environment using |Windows|.


The command prompt
------------------

The |Linux| terminal provides an environment in which you can run commands 
by typing them (most programs you are going to use on Linux are text-based):

.. code-block:: console

    $ <command>

In this tutorial ``$`` is used as a visual clue to introduce commands you
should type at the prompt. Here, ``<command>`` is just a placeholder though,
not a real command.  Once the command is typed, press :kbd:`Enter` to execute
it.  Throughout your research projects, you will probably only need ten or so
commands which allow you to move/navigate within the file hierarchy in Linux;
create, move, rename and delete files and directories; look inside and edit
text files; execute other commands.

.. note:: If you want a more in-depth explanation, have a look at
    :ref:`prompt_primer`. 


Getting familiar with basic commands
------------------------------------

Click on the terminal, so that the command prompt is active. Type the 
following (keep in mind ``$`` should not be typed, it is only a visual 
clue):

.. code-block:: console

    $ ls

The command ``ls`` stands for *list*: it allows you to list the contents of
your current directory. This directory is currently your home directory 
(i.e. your :file:`H:` drive). Many |LINUX| commands can take *options*. 
For example, a more detailed *list* output can be generated with:

.. code-block:: console

    $ ls -ao

We suggest that you keep all your simulation data in a separate 
directory. So let’s start by *making a directory* :file:`Research_Project`
(spaces are often problematic, so use an underscore instead):

.. code-block:: console

    $ mkdir Research_Project

Use the ``ls`` command to see if have indeed created a directory.

What we will do now is to *change directory* to that new one, 
*make* a subdirectory called :file:`test1` and create a text file in this 
subdirectory. You have to tell |Linux| that you want to change directory 
by using the ``cd`` command (and provide a destination):

.. code-block:: console

    $ cd Research_Project
    $ mkdir test1
    $ cd test1

How do you know that you are now in :file:`test1`? 
Use the *path to working directory* command to show your current location:

.. code-block:: console

    $ pwd
    /u/k/ggd21/Research_Project/test1

Note that this location involves a full hierarchy of folders and subfolders
(directories), starting with :file:`/` (a.k.a. the *root directory*) and
separated by :file:`/`. This hierarchy is called *path*. In the example
given above :file:`/u/k/ggd21` is the path to the personal home directory of the user with the username ggd21 (Gael Donval, a former postdoc in Tina Düren's research group, who has created a lot of these tutorials)
The path to your home directory will obviously be different and your home directory is the directory that you entered when you logged on to linux.bath.ac.uk. 
The second part, :file:`Research_Project/test1`, is where you are with respect
to your starting directory. The equivalent on a Windows / Mac would be the subfolders that you see in the File Explorer.

.. note:: :file:`/` really represents folders: :file:`foo/bar/baz` just means that
    you want to access :file:`baz` that is located in folder :file:`bar` that
    is itself located in folder :file:`foo`. There is nothing more to it.

Paths starting by :file:`/` are called *absolute paths* or *full paths* because
you can't go up from that folder: it's the root folder, the one that
contains them all. For instance ``pwd`` always gives you absolute paths:
they always start with :file:`/`. Let us walk our way through the hierarchy:

.. code-block:: console

    $ cd /
    $ ls

You are now at the root of the system: ``ls`` lists all the folders in there.
Now have a look at the first element in the ``pwd`` output you did previously.
In my case, it was simply :file:`u`: it means that there should be a folder
named :file:`u` in the root. There is indeed as you can see with ``ls``. We
can now go inside and have a look:

.. code-block:: console

    $ cd u
    $ ls

Now in my case (yours may differ), I need to look for :file:`k`: it is indeed
in there and I can ``cd`` my way into it. Proceed in the same way to
your own home folder, :file:`Research_Project` and finally :file:`test1`.

Let us try to move a level up, back to :file:`Research_Project`. You might be
tempted to type something like:

.. code-block:: console

    $ cd Research_Project

This is not going to work: list files in your current directory; 
:file:`Research_Project` is not in there! To go up in the hierarchy, type:

.. code-block:: console

    $ cd ..

Now check that you are indeed one level up. The command ``cd`` is no way 
limited to single folders: you can go two (or more) levels up trivially:

.. code-block:: console

    $ cd ../..

You can provide no path (in which case you get back to your home directory)
or more paths; you can even provide absolute paths:

.. code-block:: console

    $ cd
    $ cd Research_Project/test1
    $ cd
    $ cd /u/k/ggd21/Research_Project/test1

Absolute paths like the last one (that you have to adapt to your own home 
directory) will work wherever you happen to be in the hierarchy. Use ``pwd``
and ``ls`` at each step to be sure you understand where you are
and where you are moving to.

.. hint:: Just like :file:`..` means *one folder up*, :file:`.` means
    *current folder* and
    :file:`~` means *home directory*. The commands ``cd`` and ``cd ~`` are
    equivalent.

Now go back to :file:`test1` in :file:`Research_Project` if you are not already
in there. We are going to create a simple text file. For this we will use an
inline text editor (in other words it does have any fancy interface such as
:prog:`Word`) called :prog:`Nano`:

.. code-block:: console

    $ nano lines.txt

Within this editor you can type text, edit it, save files and so on, using a
combination of :kbd:`Ctrl` key and one of the letter options (see the lower tab
of the screen for all the options).

Type in the following point for a simple line::

    1.0  2.0
    2.0  4.0
    3.0  9.0
    4.0  16.0  

Save the file (:kbd:`Ctrl-o`) and exit the editor (:kbd:`Ctrl-x`). Use ``ls``
to double check that the file was indeed created. To check the content of that
file use the command:

.. code-block:: console

    $ less lines.txt

Press :kbd:`q` to get back to the prompt. This is also a good place in the
tutorial to go back to the :prog:`Nano` editor and play around a bit more to
understand other options.

Imagine that :file:`lines.txt` is the result of a calculation that we would like
to plot (for example it could be an adsorption isotherm). There are several
ways to do this:

* Use the program :prog:`Gnuplot` directly under |Linux| to plot the content
  of :file:`lines.txt`:

  .. code-block:: console

    $ gnuplot -p -e "plot 'lines.txt'"

* As the |Linux| file space is the same as |Windows| file space you can also
  use :prog:`Excel` (or any other suitable program) to plot the lines.

  .. note:: This is untrue for |Isambard| or any other supercomputer you might be using
    later on to run your simulations. We will provide instructions on how to access these files in the tutorial on connecting to HPC (high performance computing) facilities.

* Copy-pasting from ``less``' output is also perfectly possible.


You will often need to *copy* things around to avoid starting from scratch
every time you do something new. You can copy files the following way:

.. code-block:: console

  $ cp <original file> <new file>

For instance:

.. code-block:: console

  $ cp lines.txt new_lines.txt

To copy a file into a different directory, use:

.. code-block:: console

  $ cp <original file> <directory>/<optional file name>

To copy a whole directory, you need to instruct ``cp`` to do the copy 
recursively (i.e. copy the folder *and* its content). You can do so by
adding the ``-r`` flag:

.. code-block:: console

  $ cp -r <original directory> <new directory>


Likewise, to *remove* a file, use ``rm`` (and add ``-r`` for a directory):


.. code-block:: console

    $ rm new_lines.txt


.. warning:: You can easily delete everything you did in one click or with
    a single command. Nothing is going to ask you if you are absolutely certain
    you want to erase your whole work! Linux won't ask you if you really want to erase a file or directory unless you use the option -i which you can also use with ``cp`` to avoid accidentally overwriting a file.

.. code-block:: console

    $ rm -i new_lines.txt



.. _CLPrimer_exercices_gromacs:

Exercises
---------

Now it is time to practice these things.

#. Go into :file:`~/Research_Project`.
#. Copy :file:`test1` to :file:`test2`.
#. Remove :file:`test2/lines.txt`
#. Create :file:`test2/molecule.gro` and edit it to contain:

  .. code-block:: none

    This is Trappe TNT molecule
       16
        1TNT     C1    1   3.685   0.123   3.822
        1TNT     C2    2   3.671  -0.008   3.743
        1TNT     C3    3   3.599  -0.010   3.624
        1TNT     C4    4   3.586  -0.130   3.552
        1TNT     C5    5   3.645  -0.247   3.600
        1TNT     C6    6   3.717  -0.245   3.719
        1TNT     C7    7   3.730  -0.125   3.791
        1TNT     N8    8   3.536   0.114   3.572
        1TNT     O9    9   3.547   0.219   3.635
        1TNT    O10   10   3.473   0.113   3.467
        1TNT    N11   11   3.631  -0.374   3.523
        1TNT    O12   12   3.683  -0.477   3.565
        1TNT    O13   13   3.568  -0.376   3.418
        1TNT    N14   14   3.807  -0.123   3.919
        1TNT    O15   15   3.818  -0.018   3.982
        1TNT    O16   16   3.859  -0.226   3.961
      10.00000  10.00000  10.00000



That new file contains the coordinates of a :smallcaps:`tnt` molecule. You
are going to visualize it from |Linux| using a program called |Vmd|:

.. code-block:: console

    $ vmd

This will cause several screens to pop up. Drag the newly created 
:file:`molecule.gro` into the main area of the main window (or click
:menuselection:`Open --> New Molecule` and select that file in the hierarchy).
Now familiarise yourself with |Vmd|; play with the molecule and 
visualisation options. Particularly, find out how to save what you see on 
screen to images that you may later want to use in your report.

.. attention:: We will be using |Vmd| quite extensively. At this stage, you
    might be told something like ``Error: Can't open display`` and nothing
    would be displayed. 
    
    If you are using the command line, be sure |XQuartz|
    is running on |Mac| and don't forget to use the ``ssh -X``.

You can also install VMD on your own machine https://www.ks.uiuc.edu/Research/vmd/

Going further
-------------

If you feel you need to know more about the command line, you could start with
:ref:`prompt_primer` which contains useful information if you are:

* unsure about what you are supposed to do or type, 

* stuck with a command that does not stop, 

* trying to find out how to know more about a command.

Do not be afraid to search on the web for advice on using commands; 
there are many sites devoted to question and answer forums for |LINUX| 
or for scientific computing in general.
