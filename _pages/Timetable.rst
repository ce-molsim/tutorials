.. only:: html

    *********
    Timetable - AY 2023/24
    *********


    .. tabularcolumns:: llll

    .. _idx_schedule:


    ============== ========  ======  ============================================
    **Monday**

    11:15--12:05   6W 1.1    SC      Induction lecture
    14:15--15:05   8W 2.22   TD/MJL  Kick-off
    15:15--16:05   8W 2.222  TD      Lecture - Intermolecular interactions

    **Tuesday**

    10:15--11:05   EB 0.7    TD      Tutorial: :ref:`Introduction to Linux
                                     <sec_CLP>`
    11:15--12:05   EB 07     MJL     Lecture – Intro to molecular dynamics (MD)
    14:15--16:05   online    DS      Library skills workshop

    **Wednesday**

    10:15--11:05   EB 0.7    TD      Lecture: Intro to Monte Carlo (MC) simulations
    11:15--12:05   EB 0.7    TD      Tutorial: :ref:`Simulating Henry's coefficients
                                     <henry_in_raspa>`


    **Thursday**

    9:15--10:05    4W 1.2    MRB     Health and Safety Induction
    15:15--16:05   EB 0.7    MJL/TD  Tutorial: Uptake of land fill gases (I), material on Teams`

    **Friday**

    10:15--11:05   EB 0.7    TD/MJL  Tutorial: Uptake of landfill gases (II), material on Teams
    11:15--12:05   EB 0.7    TD/MJL  Group meeting
    ============== ========  ======  ============================================

