
.. _connect_to_Balena:

How do you access |Balena|?
===========================

.. secauthor:: Gaël Donval  <g.donval@bath.ac.uk>
               Tina Düren  <t.duren@bath.ac.uk>


One of the immediate benefits of the command-line is availability: you can
access |Balena| from literally any computer through a protocol called
|ssh| (*secure shell*).

Technically, you need a program that can speak |ssh|, proper credentials (your
university user name and password) and |Balena|'s address: 

.. tabularcolumns:: ll

============= ======================
**host name** ``balena.bath.ac.uk``
**port**      ``22``
============= ======================

There are actually many different programs you can use to connect to |Balena|:
you only need to get one of them to work. The rest of the section is dedicated
to demonstrating the log on process on a few of them. :numref:`ssh_success`
depicts what a successful connection should look like.

.. _ssh_success:
.. figure:: img/ssh_success.*
    :width: 90%

    Successful connection to |Balena|.


.. _ssh_from_CLI:

Logging in from the University computers
----------------------------------------

University-managed |Windows| computers should be provided with a shortcut to
|Balena|: you should be able to access it by searching for ``Balena`` in the
:menuselection:`Start` menu. If not, start a program called |Xming| then
launch |Kitty|. |Balena| should already be mentioned in the stored session
list. Just click it and click :menuselection:`Open`.

Enter your university username and password (nothing will be displayed) and
press :kbd:`Enter`. If the connection was a success, you are all set up.

.. note:: There are other |ssh| clients you may prefer to use such as
    |Putty|_, |MobaXterm|_ or |mRemoteNG|_: feel free to try them if you
    wish.

.. _Putty: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
.. _Kitty: https://www.fosshub.com/KiTTY.html
.. _MobaXterm: https://mobaxterm.mobatek.net
.. _mRemoteNG: https://mremoteng.org/

Logging from your |Windows| computer
------------------------------------

Download |MobaXterm|_, install it and open it. Click :menuselection:`Sessions
--> SSH` (top left corner). Type ``balena.bath.ac.uk`` in the *Remote Host*
form, click :menuselection:`Specify username`, enter your university user name,
bookmark those settings if needed and click :menuselection:`Ok`.

.. _MobaXterm: https://mobaxterm.mobatek.net

Logging in from your |Mac| or |Linux| computer
----------------------------------------------

If you are running on |Mac|, either install |MobaXterm| and follow the
indications described above or install |XQuartz|_ and launching it.

.. _XQuartz: https://www.xquartz.org/

In both |Mac| and |Linux|, just open a terminal and type the following 
replacing ``<username>`` by yours:

.. code-block:: console

    $ ssh -X <username>@balena.bath.ac.uk

Enter your password (nothing will be displayed) and press :kbd:`Enter`. If the
connection was a success, you are all set up.

.. note:: Very recent versions of |Windows| (*Fall Creator Update* or newer)
          include |OpenSSH|. To install it, head to
          :menuselection:`Start --> Settings --> Apps` and click *Manage
          optional features* under *Apps & features*.  Click *Add a feature*
          and install *OpenSSH Client*.  Once installed, you can type the
          aforementioned command in |Windows| :prog:`PowerShell`.



