
.. _ads_in_music:

*******************************************
Analysing adsorption isotherms with |Music| 
*******************************************

.. secauthor:: Tina Düren <t.duren@bath.ac.uk>
               Gaël Donval <g.donval@bath.ac.uk>

.. |IRMOF1| replace:: :name:`Irmof-1`
.. |IRMOF3| replace:: :name:`Irmof-3`
.. |CO2| replace:: :math:`\mathrm{CO}_2`
.. |GCMC| replace:: :smallcaps:`gcmc`

.. attention:: There has been an unexpected change on |Balena| making one
    of our previous configuration steps ineffective. To fix it, please do the 
    following:

    .. code-block:: console

        $ cp ~/.bashrc  ~/.bash_profile

    Then quit |Balena| and reconnect. This should fix most problems we saw
    in the previous tutorials. It is only required once and then you can
    forget about it.

The purpose of this tutorial is to see what information can be extracted
from |GCMC| calculations using the example of the study of |CO2| adsorption
in two very similar metal-organic frameworks (|MOF|\ s), |IRMOF1| and |IRMOF3|. 
You won't perform any simulation yourself this time around and we won't
describe in detail how such a simulation is practically carried out:
this will be the object of a more advanced tutorial.

**Objectives:**

* Understanding what |MOF|\ s are through two examples: |IRMOF1| and |IRMOF3|.

* Learning how to analyse |GCMC| results carried out with |Music|_ (the
  multipurpose simulation program we used to run the |GCMC| calculation).

* Getting full adsorption isotherms for |CO2|.

* Learning how to compare different |MOF|\ s with respect to their adsorption
  properties.

.. _Music: https://doi.org/10.1080/0892702031000065719


Background
----------

Metal-organic frameworks have recently attracted much attention as materials
for carbon capture materials due to their high *surface area*, low material
density and the sheer endless possibilities in combining metal centres with
organic linkers in a kind of chemical “:name:`Lego`\ ” set (see
:numref:`music_lego_mof`). The linkers can be functionalised to tailor the host
guest interactions further.

.. _music_lego_mof:

.. figure:: img/SAmof.*

    a) Schematic representation of the self-assembly process of |MOF|\ s from
    corner and bridging units. b) Building blocks for |IRMOF1| and |IRMOF3|. c)
    Structure of |IRMOF1|, one complete, larger cavity of the framework is
    shown. The transparent blue sphere was added to demonstrate the size of the
    cavity and to emphasize the open three-dimensional framework.
    Most |MOF|\ s are porous which explains why they are used for gas 
    adsorption and separation.

We are interested in |IRMOF1| and |IRMOF3| for |CO2| adsorption: as you can see
in :numref:`music_nice_mofs`, they have very similar structures: |IRMOF3|
is basically just |IRMOF1| with an extra amine group on its ligands.
This similarity can also be seen in :numref:`music_prop_mof` where we compared
different physical properties.

.. _music_nice_mofs:

.. figure:: img/IRMOFS.*

    Illustration of |IRMOF1| and |IRMOF3|.


.. |g_cm3| replace:: g/cm\ :math:`^3`
.. |A3| replace:: Å\ :math:`^3`
.. |m2_g| replace:: m\ :math:`^2`\ /g
.. |m2_cm3| replace:: m\ :math:`^2`\ /cm\ :math:`^3`


.. tabularcolumns:: lll

.. _music_prop_mof:

.. table:: Physical properties of |Irmof1| and |Irmof3|.

    ====================================== ============= =============
    *Property*                             |Irmof1|      |Irmof3|
    -------------------------------------- ------------- -------------
    density (|g_cm3|)                      0.59          0.67
    pore volume (|A3|)                     13916         13561
    molecular weight of unit-cell (g/mol)  6156          6852
    volume of unit-cell (|A3|)             17237         17067
    accessible surface area (|m2_g|)       3717          3245
    accessible surface area (|m2_cm3|)     2156          2174
    pore size (Å)                          10.8|13.9     9.1|13.9
    ====================================== ============= =============

One the one side, |IRMOF1| seems to be more porous but on the other side
the amine groups in |IRMOF3| could favour interactions with |CO2| and
increase the uptake. To determine which framework has better |CO2| adsorption
properties, we ran a few |GCMC| calculations. We obtained adsorption
isotherms for full range of pressure points and at different temperatures:
:math:`298\,\mathrm{K}`, :math:`313\,\mathrm{K}` and :math:`328\,\mathrm{K}`.

You are going to copy the result of those calculations to your directory
and we are going to analyse them.

Preparing the analysis
----------------------

:ref:`Connect <connect_to_Balena>` to |Balena| the usual way. Since this is the
last tutorial this week, you should be quite comfortable with the command-line
and with moving files around now so we won't abstract things away as much as
we've done until now. In particular, no nice archive or ``logOnComputingNode``
this time! Instead do the following:

.. code-block:: console

    $ sinteractive
    $ module purge
    $ module load group ce-molsim stack
    $ module load music/std


The command ``sinteractive`` asks the scheduler for a command-line access
on a computing node. Again, no nice green ``computing`` is displayed at the
prompt: instead a weird name should be displayed. As long as that name is 
**neither** ``balena-01`` **nor** ``balena-02``, you are on a computing node.

The ``module`` commands are used to make |Music|  available to you
(see :ref:`sec_module_management` for more details about the module system).

.. hint:: Using ``sinteractive`` is *the real way™* to ask for a computing node.
    The rule of thumb is that anything other than simple file management,
    data display and edition should occur on a computing node. That also means
    that you shouldn't have to load modules on |Balena| front nodes.

As we mentioned at the beginning, you won't be running simulations this time
(however we still need to process data, hence the use of ``sinteractive``).
You will need to copy files from the following location on |Balena| to
your scratch space:

.. code-block:: none

    /beegfs/scratch/group/ce-molsim/tutorials/music_analysis.tar.xz


Running a |GCMC| calculation in |Music|
---------------------------------------

Now if you have a look at those files you copied over, there are two folders
each containing 3 folders (one for each temperature point).
Let us start by going into :file:`IRMOF1/298K`.

|GCMC| calculations with |Music| typically starts from a so-called *control*
file, in this case, :file:`gcmc.ctr`. If you have a look in it, you should
see different sections (starting with ``------``) with an explanation for
each line. Note that ``#`` serves as a comment delimiter: everything following
a ``#`` is intended to be read by human beings and is completely ignored
by |Music|. Most elements should be self-explanatory: you are not expected
to understand everything yet and those of you who are going to use |Music|
will follow dedicated tutorials.

In ``General Information`` though, you should see a reference to *restart*
files contained in a :file:`restart/` folder: these files are written by
|Music| and as the name suggests, they are used to restart/resume a
calculation.  The same thing goes for *config* (configuration) files in
:file:`config/` that are the data files containing the results of the
calculations. These are the files we are going to postprocess and analyse.

The section ``Atomic Types`` defines the name of each atom used in the simulation
followed by a file name (:file:`.atm`). Molecules (and framework) are defined
in a very similar way in the ``Molecule Types`` section (but with :file:`.mol`
files instead).
These files exist somewhere on the disk but describing them would be going
too far for our current purpose.

The temperature of the simulation is set under ``GCMC Information`` and the
pressure points are defined in terms of *fugacity* in the same section:
they are read from :file:`fugacity_298K.dat`. If you open that file, you
should see a list of 15 fugacity points.

.. note:: At low pressure, *fugacity* and *pressure* can be considered equal
    so it is fine if you think in terms of pressure.

The calculation itself was run by executing the script :file:`run_gcmc`.  A
script is a list of commands gathered into a text file: it is equivalent to
typing each command in turn at the command line without the hassle of actually
remembering and typing all of them.

.. warning:: **Don't** run ``run_gcmc``: it would require hours to finish!

You can have a look at it though using ``less``: the ``export`` commands define
*global variables* that |Music| is going to use (in this case to know where
to look for atomic and molecular definitions). It is followed by an actual
call to |Music|: ``music_gcmc gcmc.ctr``.

.. note:: Global variables are like information shared at the terminal level. 
    All the programs run in it can then access and use them. |Music| needs
    ``ATOMSDIR``, ``MOLSDIR``, ``PMAPDIR`` and ``EMAPDIR`` to be set to an
    appropriate value to work


Getting raw isotherms
---------------------

We need to extract the isotherms from the *config* files we were mentioning
earlier. For that we need to use a new |Music| subprogram: ``music_post``
and a suitable *control* file dedicated to post-processing. In principle
the following should work:

.. code-block:: console

    $ music_post post.ctr
    Please set your ATOMSDIR environment variable

But as shown above, it doesn't because the ``ATOMSDIR`` global variable is not
set. You *could* reuse the commands from ``run_gcmc``:

.. code-block:: console

    $ export ATOMSDIR="../music_files/atoms"
    $ export MOLSDIR="../music_files/molecules"
    $ export PMAPDIR="../music_files/maps"
    $ export EMAPDIR="../music_files/maps"
    $ music_post post.ctr

These would work perfectly well! However this is very cumbersome to remember
and type every time! Instead we are going to make a script: all these commands
can be typed in a simple text file and executed all at once just by calling that
file.

To do that, just copy and paste the commands above in a new file (like 
:file:`run_post`). Be sure you don't paste the ``$`` as they only are visual
indications. Then add ``#!/bin/bash`` on its own at the top of the file.
Make it executable:

.. code-block:: console

    $ chmod u+x run_post

Finally call it:

.. code-block:: console

    $ ./run_post

Neat, isn't it? 

.. hint:: You could instead just create a copy of ``run_gcmc`` and replace
    the last line, ``music_gcmc gcmc.ctr``, by ``music_post post.ctr``.


|Music| will create three new files: 

* :file:`isotherm.IRMOF1` that does not contain any |GCMC| data and can be
  ignored (|IRMOF1| is the framework: it is never changed!).
* :file:`isotherm.CO2` that contains the raw loading/uptake (in molecules per 
  unit-cell of |IRMOF1|) vs. fugacity. The file also ends by an attempt to
  fit the :name:`Langmuir` isotherm equation to the data (not a good idea in
  this case, do you see why?). We are not going to use this file directly
  though because we need an extra processing step to get all the relevant
  data.
* :file:`IRMOF1.CO2.298K.post` contains information about energy and loading
  for each pressure point as well. 


Converting fugacities to pressures
----------------------------------

These files provide raw results out of the calculation. However
experimentalists measure *pressures* not *fugacities*. We need to convert the
quantities we calculated to quantities they measured. The program
``music_analysis`` does just that:

.. code-block:: console

    $ music_analysis < analysis.ctr

The file :file:`analysis.ctr` is quite self-explanatory: it just tells 
``music_analysis`` where to find the data it needs (i.e. the output files
of ``music_post``) and where to output its results. This will create two
files:

* :file:`energy.IRMOF1.298K.dat` which contains various energy-related 
  information; we won't use it and you can ignore it.
* :file:`adiso.IRMOF1.298K.dat` which contains the isotherm data; this is
  the file you want to use for your plots.

Plot ``N_abs`` vs. ``pressure`` and vs. ``fugacity``. Can you see why we
said pressure and fugacity are roughly the same for low values? When would
you consider them different?

.. hint:: You can create a :file:`run_analysis` script as well to automate 
    things or even better: why don't you add the command above at the end of
    :file:`run_post`? Then you'd just need to call ``./run_post`` to get
    isotherm data and post-process them to get pressures!


Understanding absolute and excess uptake
----------------------------------------

The output of a |GCMC| simulation is the absolute amount adsorbed, i.e. the total
number of adsorbate molecules present in the pore, whereas experimentally the
excess amount adsorbed is measured. The difference between the absolute amount
adsorbed and the excess amount adsorbed is shown schematically in 
:numref:`music_abs_vs_excess`.

.. _music_abs_vs_excess:

.. figure:: img/excess.*

    Schematic illustration the concept of excess mount of adsorbed particles.
    a) Free particles naturally present in the framework if there were no
    interactions (i.e. free gas in "empty box" at given pressure and temperature).
    b) *Absolute* amount of adsorbed particles: raw number of particles in the
    box. c) *Excess* amount of particles: particles that are there because of
    the interactions with the framework.

The density of fluid molecules close to the solid is higher than the bulk
density due to the intermolecular interactions between the fluid molecules and
the pore walls. Yet, even without adsorption taking place, a certain amount of
gas molecules would be present in the pore volume depending on pressure and
temperature. The
excess number of molecules, :math:`n_\mathrm{ex}`, is therefore related to 
the absolute number of molecules, :math:`n_\mathrm{abs}` the following
expression:

.. math::

    n_\mathrm{ex} = n_\mathrm{abs} - V_g \rho_g

Here, :math:`V_g` is the pore volume and :math:`\rho_g` is the molar density
of the bulk gas phase.

In practice, :math:`V_g` was calculated with the second virial coefficient as
described `here <https://doi.org/10.1021/la026399h>`_. In our case, |Music|
used the :name:`Peng-Robinson` equation of state to calculate the bulk gas 
density (case depicted by :numref:`music_abs_vs_excess`\ a). The
pore volume of |IRMOF1| and |IRMOF3| are given in :numref:`music_prop_mof`.

You may have noticed that :file:`adiso.IRMOF1.298K.dat` contains a quantity
conspicuously called ``N_ex``: this is the excess amount of adsorbed |CO2| we
were talking about. Compare ``N_abs`` and ``N_ex`` vs. ``pressure``.  Does it
make a large difference? Would ``N_abs`` be enough to compare adsorption
properties of |IRMOF1| and |IRMOF3|?


Producing snapshots
-------------------

As for the |MD| simulations, snapshots are useful for visualisation. 
However unlike in |MD| simulations, full movies aren't very informative.
Do you see why?

Here instead, we are going to look at single configurations, one for each
pressure point.  We have conveniently provided another *control* file for you
to use: :file:`snapshot.ctr`. The only line you need to focus on is this
one (near the end of the file):

.. code-block:: none

    RESTARTFILE config/CO2.IRMOF1.298K.1

It is telling |Music| which *config* file to use. To create the snapshot,
copy :file:`run_gcmc` to :file:`run_snap` and modify the last line to look
like this:

.. code-block:: none

    music_gcmc snapshot.ctr

Then save the file and execute it:

.. code-block:: console

    $ ./run_snap

|Music| will then create a new file, :file:`finalconfig.xyz`, which you
can visualise in |Vmd|. This snapshot was taken for the first *config*
file which corresponds to the first pressure value in :file:`pressure.dat`:
:math:`10\,\mathrm{kPa}`.

Give that file a relevant name (:file:`finalconfig.xyz` will be overwritten
each time you take a snapshot):

.. code-block:: console

    $ mv finalconfig.xyz snap.10kPa.298K.xyz

.. note:: When you visualise a snapshot with |Vmd|, you will notice that you 
  have :math:`\mathrm{AgNe}_2` molecules in your framework. Fear not, this is
  just a visualisation trick to differentiate the carbon and oxygen of |CO2|
  and the ones present in the framework! Said differently: the calculations
  were performed with |CO2| but we changed them to :math:`\mathrm{AgNe}_2`
  afterwards to have nice distinct colors.

.. hint:: It is easier to look at the structures if they are shown in the 
  orthographic mode (:menuselection:`Display --> Orthographic`)

To have a look at snapshots from other pressure points, go into 
:file:`pressure.dat`, choose the pressure point you want to visualise,
determine which *config* file number it corresponds to, select the correct
*config* file in :file:`snapshot.ctr` and execute your script.


Tasks
-----

You should now have all the tools (and files) to perform the following
tasks and answer the corresponding questions.

.. hint:: Though we don't provide any extra control file or script in the
    other folders, keep in mind that the ones you used until now are perfectly
    transferable to those folders. So you don't have to *really* restart from
    scratch every time.

#. Explore and discuss the adsorption mechanism of |CO2| in |IRMOF1|.

   For this task, use the simulation in |IRMOF1| at :math:`298\,\mathrm{K}` and
   investigate the pore filling mechanism with the snapshots. Where does 
   adsorption begin? As pressure increases, how do the fluid--fluid and 
   fluid--framework interactions change? Discuss the shape of the isotherm.

#. Explore and discuss the influence of temperature on |CO2| adsorption in
   |IRMOF1|. How is the isotherm affected by the increase in temperature?

   
#. Calculate and discuss the isosteric heat of adsorption
   as function of loading using the :name:`Clausius-Clapeyron` equation.
   What is its sign? Is it expected?

#. Compare the adsorption properties of |IRMOF1| and |IRMOF3|. Discuss if
   these materials show any potential as materials for carbon capture.



.. _music_comp_details:

Going further
-------------

|Music| requires only three basic elements to work: 

* atom files (:file:`.atm`) containing information about atom species; 

* molecule files (:file:`.mol`) containing the structures of the framework you
  want to study as well as the guest molecules; 

* force-field information contained in :file:`aa.interaction` (for atom--atom
  interactions), :file:`mm_sim.interactions` (for molecule--molecule interactions)
  and :file:`intra.interactions` (for intramolecular interactions).

Atom and structural details can be found in tables and in the literature.
There is little room for subjectivity in their choice usually.

The choice of proper force-fields is much more complicated though: there
are many of them derived using different methods and focusing on reproducing
a more or less limited range of physical properties.

In our case, we modelled the fluid--fluid and fluid--adsorbent interactions
using |LJ| potentials... If you consider particle :math:`i` and particle
:math:`j` at distance :math:`r_{ij}` from each other then the interaction
between :math:`i` and :math:`j` is written:

.. math::

    U(r_{ij}) = 4 \varepsilon_{ij} \left[
            \left( \frac{\sigma_{ij}}{r_{ij}} \right)^{12}
        -   \left( \frac{\sigma_{ij}}{r_{ij}} \right)^{6}
    \right]

That equation is defined by two parameters: :math:`\sigma` and
:math:`\varepsilon`. We chose our parameters from the |UFFDB|_ while the
parameters for |CO2| were taken from `this reference
<https://doi.org/10.1002/aic.690470719>`_.

.. |UFFDB| replace:: :smallcaps:`uff` database
.. _UFFDB: https://doi.org/10.1021/ja00051a040

If you have a look at them, these references only provide parameters for atoms
of the same kind: for instance, you would get :math:`\sigma_{ii}` and
:math:`\varepsilon_{ii}` for a C--C pair or an O--O pair but *not* for a C--O
pair.  To get those *cross-interactions* back, we used the usual
:name:`Lorentz--Berthelot`'s combination rule which tends to work fairly well:

.. math::

    \sigma_{ij} = \frac{\sigma_{ii}+\sigma_{jj}}{2} \quad \mathrm{and} \quad
    \varepsilon_{ij} = \sqrt{\varepsilon_{ii} \varepsilon_{jj}}


Electrostatic interactions were calculated using |EwaldSum|_.

.. |EwaldSum| replace:: :name:`Ewald` summation method
.. _EwaldSum: https://www.sciencedirect.com/science/book/9780122673511

The simulations were run for :math:`1500000` steps where each |GCMC| step
consists of either randomly translating, rotating, inserting or deleting a
molecule.
