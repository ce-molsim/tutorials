#!/bin/bash
#SBATCH --account=free
#SBATCH --job-name=TNTH2O300K
#SBATCH --output=TNTH2O300K.out
#SBATCH --error=TNTH2O300K.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --time=00:10:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cehm21@bath.ac.uk


module load gromacs/cpu/sp/5.0.2

mpdboot

grompp -v -f NVT.mdp -c box10TNT_1000Water_00.gro -o run_mpi.tpr -p system.top

mpirun -np 16 mdrun -v -s run_mpi.tpr -c box10TNT_1000Water_01.gro

mpdallexit
